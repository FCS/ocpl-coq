From iris.program_logic Require Export weakestpre.
From iris.heap_lang Require Export lang.
From iris.proofmode Require Import tactics.
From iris.heap_lang Require Import proofmode notation.
From iris.algebra Require Import auth gset.
From iris.heap_lang.lib Require Export lock.
Import uPred.

Module impl.
Definition wait_loop: val :=
  rec: "wait_loop" "x" "lk" :=
    let: "o" := !(Fst "lk") in
    if: "x" = "o"
      then () (* my turn *)
      else "wait_loop" "x" "lk".

Definition newlock' : val :=
  λ: <>, ((* owner *) ref #0, (* next *) ref #1).

Definition acquire : val :=
  rec: "acquire" "lk" :=
    let: "n" := !(Snd "lk") in
    if: CAS (Snd "lk") "n" ("n" + #1)
      then wait_loop "n" "lk"
      else "acquire" "lk".

Definition release : val :=
  λ: "lk", (Fst "lk") <- !(Fst "lk") + #1.
End impl.

Instance code : LockImpl := {|
  newlock' := impl.newlock'; acquire := impl.acquire;
  release := impl.release
|}.

(** The CMRAs we need. *)
Class tlockG Σ :=
  tlock_G :> inG Σ (authR (prodUR (optionUR (exclR natC)) (gset_disjUR nat))).
Definition tlockΣ : gFunctors :=
  #[ GFunctor (constRF (authR (prodUR (optionUR (exclR natC)) (gset_disjUR nat)))) ].

Instance subG_tlockΣ {Σ} : subG tlockΣ Σ → tlockG Σ.
Proof. by intros ?%subG_inG. Qed.

Section proof.
  Context `{!heapG Σ, !tlockG Σ}.

  Record name : Type := { nsp : namespace; ghost : gname; lo : loc; ln : loc }.

  Definition lock_inv (γ : name) (R : iProp Σ) : iProp Σ :=
    (∃ o n : nat,
      lo γ ↦ #o ∗ ln γ ↦ #n ∗
      own (ghost γ) (● (Excl' o, GSet (seq_set 0 n))) ∗
      ((own (ghost γ) (◯ (Excl' o, ∅)) ∗ R) ∨
       own (ghost γ) (◯ (∅, GSet {[ o ]}))))%I.

  Definition is_lock (γ : name) (lk : val) (R : iProp Σ) : iProp Σ :=
    (⌜heapN ⊥ nsp γ⌝ ∗ heap_ctx ∗
       ⌜lk = (lo γ, ln γ)%V⌝ ∗ inv (nsp γ) (lock_inv γ R))%I.

  Definition issued (γ : name) (lk : val) (x : nat) (R : iProp Σ) : iProp Σ :=
    (⌜heapN ⊥ nsp γ⌝ ∗ heap_ctx ∗
       ⌜lk = (lo γ, ln γ)%V⌝ ∗ inv (nsp γ) (lock_inv γ R) ∗
       own (ghost γ) (◯ (∅, GSet {[ x ]})))%I.

  Definition locked (γ : name) : iProp Σ :=
    (∃ o, own (ghost γ) (◯ (Excl' o, ∅)))%I.

  Global Instance lock_inv_ne n γ : Proper (dist n ==> dist n) (lock_inv γ).
  Proof. solve_proper. Qed.
  Global Instance is_lock_ne γ lk n : Proper (dist n ==> dist n) (is_lock γ lk).
  Proof. solve_proper. Qed.
  Global Instance is_lock_persistent γ lk R : PersistentP (is_lock γ lk R).
  Proof. apply _. Qed.
  Global Instance locked_timeless γ : TimelessP (locked γ).
  Proof. apply _. Qed.

  Lemma locked_exclusive (γ : name) : locked γ -∗ locked γ -∗ False.
  Proof.
    iDestruct 1 as (o1) "H1". iDestruct 1 as (o2) "H2".
    iDestruct (own_valid_2 with "H1 H2") as %[[] _].
  Qed.

  Lemma newlock'_spec p N (R : iProp Σ) :
    heapN ⊥ N →
    {{{ heap_ctx }}} newlock' () @ p; ⊤
    {{{ lk γ, RET lk; is_lock γ lk R ∗ locked γ }}}.
  Proof.
    iIntros (? Φ) "#Hh HΦ". rewrite -wp_fupd.
    wp_seq. wp_alloc lo as "Hlo". wp_alloc ln as "Hln".
    iMod (own_alloc (● (Excl' 0%nat, GSet (seq_set 0 1)) ⋅
      ◯ (∅, GSet {[0%nat]}) ⋅ ◯ (Excl' 0%nat, ∅))) as (γlk) "((Hγ1 & Hγ2) & Hγ3)".
    { by rewrite -assoc -auth_frag_op -auth_both_op pair_op left_id right_id
      auth_valid_discrete /=  prod_included -gset_disj_union //= right_id. }
    set γ := {| nsp := N; ghost := γlk; lo := lo; ln := ln |}.
    iMod (inv_alloc N _ (lock_inv γ R) with "[-HΦ Hγ3]").
    { iNext. rewrite /lock_inv. iExists 0%nat, 1%nat. iFrame. by iRight. }
    iModIntro. iApply ("HΦ" $! (lo, ln)%V γ). iSplitR "Hγ3".
    by rewrite/is_lock; eauto. by iExists 0%nat.
  Qed.

  Lemma wait_loop_spec p γ lk x R :
    {{{ issued γ lk x R }}} impl.wait_loop #x lk @ p; ⊤
    {{{ RET (); locked γ ∗ R }}}.
  Proof.
    iIntros (Φ) "Hl HΦ". iDestruct "Hl" as "(% & #? & % & #? & Ht)".
    iLöb as "IH". wp_rec. subst. wp_let. wp_proj. wp_bind (! _)%E.
    iInv (nsp γ) as (o n) "(Hlo & Hln & Ha)" "Hclose".
    wp_load. destruct (decide (x = o)) as [->|Hneq].
    - iDestruct "Ha" as "[Hainv [[Ho HR] | Haown]]".
      + iMod ("Hclose" with "[Hlo Hln Hainv Ht]") as "_".
        { iNext. iExists o, n. iFrame. eauto. }
        iModIntro. wp_let. wp_op=>[_|[]] //.
        wp_if.
        iApply ("HΦ" with "[-]"). rewrite /locked. iFrame. eauto.
      + iDestruct (own_valid_2 with "Ht Haown") as % [_ ?%gset_disj_valid_op].
        set_solver.
    - iMod ("Hclose" with "[Hlo Hln Ha]").
      { iNext. iExists o, n. by iFrame. }
      iModIntro. wp_let. wp_op=>[[/Nat2Z.inj //]|?].
      wp_if. iApply ("IH" with "Ht"). iNext. by iExact "HΦ".
  Qed.

  Lemma acquire_spec p γ lk R :
    {{{ is_lock γ lk R }}} acquire lk @ p; ⊤ {{{ RET (); locked γ ∗ R }}}.
  Proof.
    iIntros (ϕ) "Hl HΦ". iDestruct "Hl" as "(% & #? & % & #?)".
    iLöb as "IH". wp_rec. wp_bind (! _)%E. subst. wp_proj.
    iInv (nsp γ) as (o n) "[Hlo [Hln Ha]]" "Hclose".
    wp_load. iMod ("Hclose" with "[Hlo Hln Ha]") as "_".
    { iNext. iExists o, n. by iFrame. }
    iModIntro. wp_let. wp_proj. wp_op.
    wp_bind (CAS _ _ _).
    iInv (nsp γ) as (o' n') "(>Hlo' & >Hln' & >Hauth & Haown)" "Hclose".
    destruct (decide (#n' = #n))%V as [[= ->%Nat2Z.inj] | Hneq].
    - wp_cas_suc.
      iMod (own_update with "Hauth") as "[Hauth Hofull]".
      { eapply auth_update_alloc, prod_local_update_2.
        eapply (gset_disj_alloc_empty_local_update _ {[ n ]}).
        apply (seq_set_S_disjoint 0). }
      rewrite -(seq_set_S_union_L 0).
      iMod ("Hclose" with "[Hlo' Hln' Haown Hauth]") as "_".
      { iNext. iExists o', (S n).
        rewrite Nat2Z.inj_succ -Z.add_1_r. by iFrame. }
      iModIntro. wp_if.
      iApply (wait_loop_spec p γ (lo γ, ln γ) with "[-HΦ]").
      + rewrite /issued; eauto 10.
      + by iNext.
    - wp_cas_fail.
      iMod ("Hclose" with "[Hlo' Hln' Hauth Haown]") as "_".
      { iNext. iExists o', n'. by iFrame. }
      iModIntro. wp_if. by iApply "IH"; auto.
  Qed.

  Lemma release_spec p γ lk R :
    {{{ is_lock γ lk R ∗ locked γ ∗ R }}} release lk @ p; ⊤
    {{{ RET (); True }}}.
  Proof.
    iIntros (Φ) "(Hl & Hγ & HR) HΦ". iDestruct "Hl" as "(% & #? & % & #?)"; subst.
    iDestruct "Hγ" as (o) "Hγo".
    rewrite /release. wp_let. wp_proj. wp_proj. wp_bind (! _)%E.
    iInv (nsp γ) as (o' n) "(>Hlo & >Hln & >Hauth & Haown)" "Hclose".
    wp_load.
    iDestruct (own_valid_2 with "Hauth Hγo") as
      %[[<-%Excl_included%leibniz_equiv _]%prod_included _]%auth_valid_discrete_2.
    iMod ("Hclose" with "[Hlo Hln Hauth Haown]") as "_".
    { iNext. iExists o, n. by iFrame. }
    iModIntro. wp_op.
    iInv (nsp γ) as (o' n') "(>Hlo & >Hln & >Hauth & Haown)" "Hclose".
    wp_store.
    iDestruct (own_valid_2 with "Hauth Hγo") as
      %[[<-%Excl_included%leibniz_equiv _]%prod_included _]%auth_valid_discrete_2.
    iDestruct "Haown" as "[[Hγo' _]|?]".
    { iDestruct (own_valid_2 with "Hγo Hγo'") as %[[] ?]. }
    iMod (own_update_2 with "Hauth Hγo") as "[Hauth Hγo]".
    { apply auth_update, prod_local_update_1.
      by apply option_local_update, (exclusive_local_update _ (Excl (S o))). }
    iMod ("Hclose" with "[Hlo Hln Hauth Haown Hγo HR]") as "_"; last by iApply "HΦ".
    iNext. iExists (S o), n'.
    rewrite Nat2Z.inj_succ -Z.add_1_r. iFrame. iLeft. by iFrame.
  Qed.
End proof.

Typeclasses Opaque is_lock issued locked.

Definition proof `{!heapG Σ, !tlockG Σ} : lock Σ :=
  {| lock.locked_exclusive := locked_exclusive; lock.newlock'_spec := newlock'_spec;
     lock.acquire_spec := acquire_spec; lock.release_spec := release_spec |}.
